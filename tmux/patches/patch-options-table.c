$NetBSD$

--- options-table.c.orig	2016-01-21 00:00:28.000000000 +0000
+++ options-table.c
@@ -290,6 +290,12 @@ const struct options_table_entry options
 	  .default_num = 0
 	},
 
+	{ .name = "pane-border-ascii",
+	  .type = OPTIONS_TABLE_FLAG,
+	  .scope = OPTIONS_TABLE_SESSION,
+	  .default_num = 0
+	},
+
 	{ .name = "prefix",
 	  .type = OPTIONS_TABLE_KEY,
 	  .scope = OPTIONS_TABLE_SESSION,
